#include "ground.h"

ground::ground(SDL_Surface* window_surface_ptr) {
    this->window_surface_ptr_ = window_surface_ptr;
    sheperd = nullptr;
    score = 0;
}

ground::~ground() {
    for (int i = 0; i < animals.size(); i++) {
        delete animals[i];
    }
    animals.clear();
    delete(sheperd);
}

void ground::add_animal(moving_object* a){
    animals.push_back(a);
    a->setGround(this);
}

void ground::add_playable(playable_character* p) {
    this->sheperd = p;
    p->setGround(this);
}

moving_object* ground::getClosestEntity(position it) {
    moving_object* res = nullptr;
    int dist = INT_MAX;
    for (int i = 0; i < animals.size(); i++) {
        int temp = animals[i]->getPos().distance(it);
        if (temp < dist && temp != 0){
            res = animals[i];
            dist = temp;
        }
            
    }
    return res;
}

moving_object* ground::getClosestEntity(position it, std::string prop){
    moving_object* res = nullptr;
    int dist = INT_MAX;
    for (int i = 0; i < animals.size(); i++) {
        int temp = animals[i]->getPos().distance(it);
        if (temp < dist && temp != 0 && animals[i]->hasProperty(prop))
            res = animals[i];
    }
    return res;
}

position ground::getPlayerPosition(){
    return sheperd->getPos();
}

int ground::getScore(std::string prop){
    score = 0;
    for (int i = 0; i < animals.size(); i++) {
        if (animals[i]->hasProperty(prop))
            score += 1000;
    }
    return score;
}

void ground::killAnimal(position p){
    for(int i = 0; i < animals.size(); i++){
        if (animals[i]->getPos().distance(p) == 0)
            animals[i]->kill();
    }
}

void ground::update() {
    SDL_FillRect(window_surface_ptr_, NULL, SDL_MapRGB(window_surface_ptr_->format, 0, 255, 0));
    for (int i = 0; i < animals.size(); i++) {
        position pos = animals[i]->getPos();
        moving_object* closest_animal = getClosestEntity(pos);
        score = 0;
        if(animals[i]->hasProperty("alive")){
            if(closest_animal->getPos().distance(pos) < 100){
                animals[i]->interact(closest_animal);
            }
            animals[i]->move();
            animals[i]->draw();
        }  
    }
    sheperd->move();
    sheperd->draw();
}

void ground::update_player(SDL_Keycode key) {
    sheperd->setKey(key);
}
