#pragma once
#include "animal.h"

// class sheep, derived from animal
class sheep : public animal {
private:
    int growth = 150;
    int fertility_cooldown = 10;
public:
    sheep(const std::string& file_path, SDL_Surface* window_surface_ptr,int x = 0,int y = 0, int speed = 6, ground* terrain = nullptr);
    void move();
    void interact(interacting_object* other);
    // implement functions that are purely virtual in base class
};