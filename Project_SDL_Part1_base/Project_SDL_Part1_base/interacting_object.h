#pragma once
#include <vector>
#include <string>
class interacting_object {
private:
    std::vector<std::string> properties;
public:
    interacting_object();
    ~interacting_object();
    void addProperty(std::string prop);
    void kill();
    int hasProperty(std::string prop);
    virtual void interact(interacting_object* other) = 0;
};
