#include "moving_object.h"
#include "ground.h"

moving_object::moving_object(const std::string& file_path, SDL_Surface* window_surface_ptr, int x, int y, int speed, ground* terrain) :rendered_object(file_path, window_surface_ptr, x, y) {
    setSpeed(speed);
    addProperty("alive");
    this->terrain = terrain;
}

moving_object::~moving_object() {}

void moving_object::setSpeed(int speed){
    this->speed = speed;
}

int moving_object::getSpeed(){
    return speed;
}

void moving_object::setGround(ground* terrain){
    this->terrain = terrain;
}

ground* moving_object::getGround(){
    return this->terrain;
}
