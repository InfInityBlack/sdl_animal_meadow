#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include "interacting_object.h"
#include "position.h"

class rendered_object : public interacting_object{
private:
    SDL_Surface* window_surface_ptr_; 
    SDL_Surface* image_ptr_;
    position pos;
    

public:
    rendered_object(const std::string& file_path, SDL_Surface* window_surface_ptr,int x = 0, int y = 0);
    ~rendered_object();
    position getPos();
    SDL_Surface* getSurface();
    void changeImage(const std::string& file_path);
    void setPos(int x,int y);
    void draw();
    virtual void interact(interacting_object* other) = 0;
};