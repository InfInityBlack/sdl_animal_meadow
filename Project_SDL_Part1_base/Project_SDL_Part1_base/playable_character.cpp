#include "playable_character.h"

playable_character::playable_character(const std::string& file_path, SDL_Surface* window_surface_ptr, int x, int y, int speed, ground* terrain): moving_object(file_path,window_surface_ptr,x,y,speed,terrain){
	key = SDLK_0;
}

playable_character::~playable_character(){}

void playable_character::setKey(SDL_Keycode key){
	this->key = key;
}

SDL_Keycode playable_character::getKey(){
	return this->key;
}
