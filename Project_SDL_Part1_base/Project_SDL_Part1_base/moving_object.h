#pragma once
#include "rendered_object.h"
#include "declarations.h"

class moving_object : public rendered_object{
private:
    int speed;
    ground* terrain;
public:
    moving_object(const std::string& file_path, SDL_Surface* window_surface_ptr, int x = 0, int y = 0, int speed = 5, ground* terrain = nullptr);
    ~moving_object(); 

    void setSpeed(int speed);
    int getSpeed();
    void setGround(ground* terrain);
    ground* getGround();
    virtual void move() = 0;
    virtual void interact(interacting_object* other) = 0;
};