#include "sheperd.h"

sheperd::sheperd(const std::string& file_path, SDL_Surface* window_surface_ptr, int x, int y, int speed, ground* terrain):playable_character(file_path,window_surface_ptr,x,y,speed,terrain){}

void sheperd::move(){
    SDL_Keycode key = getKey();
    position pos = getPos();
    switch (key) {
    case SDLK_z:
        setPos(pos.getPosX(), pos.getPosY() - getSpeed());
        break;
    case SDLK_q:
        setPos(pos.getPosX() - getSpeed(), pos.getPosY());
        break;
    case SDLK_s:
        setPos(pos.getPosX(), pos.getPosY() + getSpeed());
        break;
    case SDLK_d:
        setPos(pos.getPosX() + getSpeed(), pos.getPosY());
        break;
    default:
        break;
    }
}

void sheperd::interact(interacting_object* other){
    //TODO
}
