#include "sheep.h"
#include "ground.h"


sheep::sheep(const std::string& file_path, SDL_Surface* window_surface_ptr,int x,int y, int speed, ground* terrain) :animal(file_path, window_surface_ptr,x,y,speed,terrain) {
    this->addProperty("prey");
    this->addProperty("sheep");
    int rand = std::rand();
    rand < RAND_MAX / 2 ? this->addProperty("male") : this->addProperty("female");
}

void sheep::move() {
    if(this->hasProperty("lamb") && !this->hasProperty("adult")){
        if (growth == 0){
            changeImage("../../../../media/sheep.png");
            this->addProperty("adult");
        }
        else
            growth -= 1;
    }
    position pos = getPos();
    moving_object* other = getGround()->getClosestEntity(pos, "predator");
    if(other){
        position goal = other->getPos();
        int dist = pos.distance(goal);
        if (dist < 150) {
            int movX;
            int movY;
            int speed = getSpeed();
            goal.getPosX() < pos.getPosX() ? movX = speed * 1.5 : movX = -speed * 1.5;
            goal.getPosY() < pos.getPosY() ? movY = speed * 1.5 : movY = -speed * 1.5;
            setPos(pos.getPosX() + movX, pos.getPosY() + movY);
        }
        else {
            int randX = std::rand();
            int randY = std::rand();
            if (randX < RAND_MAX / 2)
                randX = -randX;
            if (randY < RAND_MAX / 2)
                randY = -randY;
            setPos(pos.getPosX() + randX % getSpeed(), pos.getPosY() + randY % getSpeed());
        }
    }
    else {
        int randX = std::rand();
        int randY = std::rand();
        if (randX < RAND_MAX / 2)
            randX = -randX;
        if (randY < RAND_MAX / 2)
            randY = -randY;
        setPos(pos.getPosX() + randX % getSpeed(), pos.getPosY() + randY % getSpeed());
    }
    fertility_cooldown-=1;
}

void sheep::interact(interacting_object* other){
    if(other->hasProperty("sheep")){
        if (other->hasProperty("male") && this->hasProperty("female") && fertility_cooldown <= 0){
            fertility_cooldown = 100;
            moving_object* lamb = new sheep("../../../../media/lamb.png",getSurface() , this->getPos().getPosX() + 2, this->getPos().getPosY() + 2);
            lamb->addProperty("lamb");
            this->getGround()->add_animal(lamb);
        }
    }
}