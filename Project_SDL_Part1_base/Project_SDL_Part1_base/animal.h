#pragma once
#include "moving_object.h"


class animal : public moving_object{
public:
    animal(const std::string& file_path, SDL_Surface* window_surface_ptr, int x = 0, int y = 0, int speed = 1., ground* terrain = nullptr);

    ~animal();
    virtual void move() = 0;
    virtual void interact(interacting_object* other) = 0;
};