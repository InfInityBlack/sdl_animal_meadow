#include "sheperd_dog.h"
#include "ground.h"


sheperd_dog::sheperd_dog(const std::string& file_path, SDL_Surface* window_surface_ptr, int x, int y, int speed, ground* terrain) :animal(file_path, window_surface_ptr, x, y, speed, terrain) {
    addProperty("defender");
}

void sheperd_dog::move() {
    position sheperd = getGround()->getPlayerPosition();
    position pos = getPos();
    if(pos.distance(sheperd) > 50){
        int movX;
        int movY;
        sheperd.getPosX() < pos.getPosX() ? movX = -getSpeed() : movX = getSpeed();
        sheperd.getPosY() < pos.getPosY() ? movY = -getSpeed() : movY = getSpeed();
        setPos(pos.getPosX() + movX, pos.getPosY() + movY);
    }
    //le déplacement vers le sheperd fonctionne mais il ne tourne pas bien autour
    else{
        if(pos.getPosX() > sheperd.getPosX())//si a droite
            setPos(sheperd.getPosX(), sheperd.getPosY() - 40);//bas  
        if(pos.getPosX() < sheperd.getPosX())//si a gauche
            setPos(sheperd.getPosX(), sheperd.getPosY() + 40);//haut
        if(pos.getPosY() > sheperd.getPosY())//si en haut
            setPos(sheperd.getPosX(), sheperd.getPosY() + 40);//droite
        else
            setPos(sheperd.getPosX(), sheperd.getPosY() - 40);//gauche
    }
}

void sheperd_dog::interact(interacting_object* other){
    //pas d'interaction spécifique pour le sheperd dog
}
