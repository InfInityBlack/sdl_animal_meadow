#pragma once
#include "moving_object.h"

class playable_character : public moving_object {
private:
    SDL_Keycode key;
public:
    playable_character(const std::string& file_path, SDL_Surface* window_surface_ptr, int x = 0, int y = 0, int speed = 10, ground* terrain = nullptr);
    ~playable_character();
    void setKey(SDL_Keycode key);
    SDL_Keycode getKey();
    virtual void move() = 0;
    virtual void interact(interacting_object* other) = 0;
};