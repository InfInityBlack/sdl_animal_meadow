﻿// SDL_Test.cpp: Definiert den Einstiegspunkt für die Anwendung.
//

#include "Project_SDL1.h"

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <numeric>
#include <random>
#include <string>

void init() {
  // Initialize SDL
  if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO) < 0)
    throw std::runtime_error("init():" + std::string(SDL_GetError()));

  // Initialize PNG loading
  int imgFlags = IMG_INIT_PNG;
  if (!(IMG_Init(imgFlags) & imgFlags))
    throw std::runtime_error("init(): SDL_image could not initialize! "
                             "SDL_image Error: " +
                             std::string(IMG_GetError()));
  std::srand((unsigned int)time(NULL));
}

namespace {
// Defining a namespace without a name -> Anonymous workspace
// Its purpose is to indicate to the compiler that everything
// inside of it is UNIQUELY used within this source file.

SDL_Surface* load_surface_for(const std::string& path,
                              SDL_Surface* window_surface_ptr) {
    SDL_Surface* img_pointer = IMG_Load(path.c_str());
    SDL_Surface* img = SDL_ConvertSurface(img_pointer,window_surface_ptr->format,0);
    return img;
  // Helper function to load a png for a specific surface
  // See SDL_ConvertSurface
}
} // namespace

ground::ground(){
    window_surface_ptr_ = nullptr;
}

ground::ground(SDL_Surface* window_surface_ptr) {
  this->window_surface_ptr_ = window_surface_ptr;
}

ground::~ground() { 
    for (int i = 0; i < animals.size();i++) {
        delete animals[i];
    }
    animals.clear();
}

void ground::add_animal(animal* a) { 
    animals.push_back(a);
}

void ground::update(){
    SDL_FillRect(window_surface_ptr_, NULL,SDL_MapRGB(window_surface_ptr_->format,0,255,0));
    for (int i = 0; i < animals.size();i++) {
        animals[i]->move();
        animals[i]->draw();
    }
}

sheep::sheep(const std::string& file_path, SDL_Surface* window_surface_ptr):animal(file_path, window_surface_ptr){}

void sheep::move(){
    int randX = std::rand();
    int randY = std::rand();
    if (randX < RAND_MAX / 2)
        randX = -randX;
    if (randY < RAND_MAX / 2)
        randY = -randY;
    this->setPosX(this->getPosX() + randX%11);
    this->setPosY(this->getPosY() + randY%11);
}

animal::animal(const std::string& file_path, SDL_Surface* window_surface_ptr){
    this->window_surface_ptr_ = window_surface_ptr;
    this->image_ptr_ = load_surface_for(file_path, window_surface_ptr_);
    setPosX(300);
    setPosY(300);
}

animal::~animal(){
    SDL_FreeSurface(image_ptr_);
}

void animal::draw(){
    SDL_Rect temp;
    temp.x = getPosX();
    temp.y = getPosY();
    temp.h = image_ptr_->h;
    temp.w = image_ptr_->w;
    SDL_BlitScaled(image_ptr_, NULL, window_surface_ptr_, &temp);
}

void animal::setPosX(int x){
    this->x = x;
}

void animal::setPosY(int y){
    this->y = y;
}

int animal::getPosX(){
    return this->x;
}

int animal::getPosY(){
    return this->y;
}

application::application(unsigned n_sheep, unsigned n_wolf)
{
    window_ptr_ = SDL_CreateWindow("beep beep am a sheep",100,100,640,640,0);
    window_surface_ptr_ = SDL_GetWindowSurface(window_ptr_);
    prairie = ground(window_surface_ptr_);
    for(int i =0;i<n_sheep;i++){
        prairie.add_animal(new sheep("../../../../media/sheep.png",window_surface_ptr_));
        //"D:/Fork/sdl_animal_meadow/Project_SDL_Part1_base/media/sheep.png"
    }
}

application::~application(){
    SDL_FreeSurface(window_surface_ptr_);
    SDL_DestroyWindow(window_ptr_);
}

int application::loop(unsigned period){
    unsigned duration = period * 1000;
    while(SDL_GetTicks()<duration){
        prairie.update();
        SDL_UpdateWindowSurface(window_ptr_);
        SDL_Delay(100);
    }
    return 0;
}
