#include "position.h"

position::position(int x, int y){
	this->x = x;
	this->y = y;
}

void position::setPosX(int posX){
	x = posX;
}

void position::setPosY(int posY){
	y = posY;
}

int position::getPosX(){
	return x;
}

int position::getPosY(){
	return y;
}

int position::distance(position other){
	return sqrt(pow(getPosX()-other.getPosX(),2.)+pow(getPosY()-other.getPosY(),2.));
}
