#include "wolf.h"
#include "ground.h"


wolf::wolf(const std::string& file_path, SDL_Surface* window_surface_ptr,int x,int y, int speed, ground* terrain) :animal(file_path, window_surface_ptr,x,y,speed,terrain) {
    this->addProperty("predator");
}

void wolf::move() {
    if (life_counter <= 0)
        this->kill();
    if(eat_cooldown == 0){
        position pos = getPos();
        moving_object* other = getGround()->getClosestEntity(pos, "prey");
        moving_object* defender = getGround()->getClosestEntity(pos, "defender");
        int randX = std::rand();
        int randY = std::rand();
        randX /= RAND_MAX - 0.5;
        randY /= RAND_MAX - 0.5;
        if (other) {
            position goal = other->getPos();
            int movX;
            int movY;
            int speed = getSpeed();
            goal.getPosX() < pos.getPosX() ? movX = -speed + speed*randX : movX = speed + speed * randX;
            goal.getPosY() < pos.getPosY() ? movY = -speed + speed*randY : movY = speed + speed * randY;
            if(defender){
                position evade = defender->getPos();
                if(evade.distance(pos) > 100){
                    evade.getPosX() < pos.getPosX() ? movX += speed * 0.2 : movX -= speed * 0.2;
                    evade.getPosY() < pos.getPosY() ? movY += speed * 0.2 : movY -= speed * 0.2;
                }
                else{
                    evade.getPosX() < pos.getPosX() ? movX = speed * 0.5 : movX = -speed * 0.5;
                    evade.getPosY() < pos.getPosY() ? movY = speed * 0.5 : movY = -speed * 0.5;
                }
            }
            setPos(pos.getPosX() + movX, pos.getPosY() + movY);
        }
    }
    else
        eat_cooldown--;
    life_counter--;
}

void wolf::interact(interacting_object* other){
    if(other->hasProperty("prey")){
        other->kill();
        life_counter = 50;
        eat_cooldown = 5;
    }
}