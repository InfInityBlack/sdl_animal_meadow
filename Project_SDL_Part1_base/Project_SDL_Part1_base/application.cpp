#include "application.h"
#include "sheep.h"
#include "wolf.h"
#include "sheperd.h"
#include "sheperd_dog.h"

void init() {
    // Initialize SDL
    if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO) < 0)
        throw std::runtime_error("init():" + std::string(SDL_GetError()));

    // Initialize PNG loading
    int imgFlags = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlags) & imgFlags))
        throw std::runtime_error("init(): SDL_image could not initialize! "
            "SDL_image Error: " +
            std::string(IMG_GetError()));
    std::srand((unsigned int)time(NULL));
}

application::application(unsigned n_sheep, unsigned n_wolf)
{
    window_ptr_ = SDL_CreateWindow("beep beep am a sheep", 100, 100, 2000, 1000, 0);
    window_surface_ptr_ = SDL_GetWindowSurface(window_ptr_);
    prairie = ground(window_surface_ptr_);
    for (int i = 0; i < n_sheep; i++) {
        prairie.add_animal(new sheep("../../../../media/sheep.png", window_surface_ptr_,250,250));
    }
    for (int i = 0; i < n_wolf; i++) {
        prairie.add_animal(new wolf("../../../../media/wolf.png", window_surface_ptr_,500,300));
    }
    prairie.add_animal(new sheperd_dog("../../../../media/sheperd_dog.png", window_surface_ptr_,300,300));
    prairie.add_playable(new sheperd("../../../../media/sheperd.png", window_surface_ptr_,500,500));
}

application::~application() {
    SDL_FreeSurface(window_surface_ptr_);
    SDL_DestroyWindow(window_ptr_);
}

int application::loop(unsigned period) {
    unsigned duration = period * 1000;
    SDL_Event event;
    SDL_Keycode key;
    while (SDL_GetTicks() < duration) {
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_KEYDOWN:
                key = event.key.keysym.sym;
                prairie.update_player(key);
                break;
            case SDL_KEYUP:
                prairie.update_player(SDLK_0);
                break;
            case SDL_QUIT:
                SDL_Quit();
                return 0;
            default:
                break;
            }
        }
        prairie.update();  
        SDL_UpdateWindowSurface(window_ptr_);
        std::cout << "score: " + std::to_string(prairie.getScore("sheep"))<< "\n";
        SDL_Delay(100);
    }
    return 0;
}
