#pragma once
#include <SDL.h>
#include "animal.h"
#include "playable_character.h"
#include <vector>
#include "declarations.h"

class ground {
private:
    SDL_Surface* window_surface_ptr_;
    std::vector<moving_object*> animals;
    playable_character* sheperd;
    int score;
public:
    ground(SDL_Surface* window_surface_ptr = nullptr);
    ~ground();
    void add_animal(moving_object* a);
    void add_playable(playable_character* p);
    moving_object* getClosestEntity(position it);
    moving_object* getClosestEntity(position it, std::string prop);
    position getPlayerPosition();
    int getScore(std::string prop);
    void killAnimal(position p);
    void update();
    void update_player(SDL_Keycode key);
};