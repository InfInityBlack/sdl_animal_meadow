#pragma once
#include "playable_character.h"
// class sheep, derived from animal
class sheperd : public playable_character {
public:
    sheperd(const std::string& file_path, SDL_Surface* window_surface_ptr,int x = 0, int y = 0,int speed = 10,ground* terrain = nullptr);
    void move();
    void interact(interacting_object* other);
};