#include "interacting_object.h"

interacting_object::interacting_object() {}

interacting_object::~interacting_object() {}

void interacting_object::addProperty(std::string prop) {
	if (!hasProperty(prop))
		properties.push_back(prop);
}

void interacting_object::kill(){
	properties.clear();
}

int interacting_object::hasProperty(std::string prop) {
	for (auto s : properties) {
		if (s.compare(prop) == 0)
			return 1;
	}
	return 0;
}
