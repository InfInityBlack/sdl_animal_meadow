#pragma once
#include <cmath>

class position{
private:
	int x;
	int y;
public:
	position(int x = 0, int y = 0);
	void setPosX(int posX);
	void setPosY(int posY);
	int getPosX();
	int getPosY();
	int distance(position other);
};