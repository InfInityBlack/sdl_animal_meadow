#include "rendered_object.h"


namespace {
    // Defining a namespace without a name -> Anonymous workspace
    // Its purpose is to indicate to the compiler that everything
    // inside of it is UNIQUELY used within this source file.

    SDL_Surface* load_surface_for(const std::string& path,
        SDL_Surface* window_surface_ptr) {
        SDL_Surface* img_pointer = IMG_Load(path.c_str());
        SDL_Surface* img = SDL_ConvertSurface(img_pointer, window_surface_ptr->format, 0);
        return img;
        // Helper function to load a png for a specific surface
        // See SDL_ConvertSurface
    }
} // namespace

rendered_object::rendered_object(const std::string& file_path, SDL_Surface* window_surface_ptr, int x, int y){
    this->window_surface_ptr_ = window_surface_ptr;
    this->image_ptr_ = load_surface_for(file_path, window_surface_ptr_);
    pos.setPosX(x);
    pos.setPosY(y);
}

rendered_object::~rendered_object(){
    SDL_FreeSurface(image_ptr_);
}

position rendered_object::getPos(){
    return pos;
}

SDL_Surface* rendered_object::getSurface(){
    return window_surface_ptr_;
}

void rendered_object::changeImage(const std::string& file_path){
    this->image_ptr_ = load_surface_for(file_path, window_surface_ptr_);
}

void rendered_object::setPos(int x,int y){
    pos.setPosX(x);
    pos.setPosY(y);
}

void rendered_object::draw() {
    SDL_Rect temp;
    temp.x = pos.getPosX();
    temp.y = pos.getPosY();
    temp.h = image_ptr_->h;
    temp.w = image_ptr_->w;
    SDL_BlitScaled(image_ptr_, NULL, window_surface_ptr_, &temp);
}
