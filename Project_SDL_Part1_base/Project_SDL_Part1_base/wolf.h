#pragma once
#include "animal.h"

// class sheep, derived from animal
class wolf : public animal {
private:
    int life_counter = 250;
    int eat_cooldown = 15;
public:
    wolf(const std::string& file_path, SDL_Surface* window_surface_ptr,int x = 100,int y = 100, int speed = 17, ground* terrain = nullptr);
    void move();
    void interact(interacting_object* other);
    // implement functions that are purely virtual in base class
};