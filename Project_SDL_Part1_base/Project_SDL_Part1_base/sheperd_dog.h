#pragma once
#include "animal.h"

// class sheep, derived from animal
class sheperd_dog : public animal {
public:
    sheperd_dog(const std::string& file_path, SDL_Surface* window_surface_ptr, int x = 100, int y = 100, int speed = 15, ground* terrain = nullptr);
    void move();
    void interact(interacting_object* other);
    // implement functions that are purely virtual in base class
};